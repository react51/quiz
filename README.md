# Quiz

## Juan Luis Belda Ortolá

### Introducción

Quiz es un juego de preguntas y respuestas.

El juego consiste en contestar las 10 preguntas del genero que tu quieras. Tiene 3 niveles de dificultad.

También puede no elegir categoría o dificultad y Quiz te hará 10 preguntas aleatorios de cualquier nivel y categoría.
 

### Explicacion en profundidad de la aplicación

La aplicación está compuesta por React con Redux. Provee las preguntas de un API pública
https://opentdb.com/api_config.php, que devuelve un JSON segun la configuraion que le pases por la ruta de la petición GET.

  

Quiz presenta cuando arranca la aplicacion un formulario sencillo donde puede introducir tu nombre. Y seleccionar la categoría y la dificultad.
Cuando presionas en "START", ejecuta el primer action y el middleware se encarga de generar la URL para la petición al api. Y con sus respueta provee al store de todo lo necesario para que el APP trabaje.

La aplicacion esta presentada con <a href="https://material-ui.com/es/">Material.UI</a>

       "@material-ui/core": "^4.9.0",
       "@material-ui/icons": "^4.5.1",

### Detalles por componente

#### login.js

La clase QuizLogin se encargará de manejar los datos del formulario. Sirviéndose del Store importa el objeto login con los campos necesarios.

```json
       login: {
           username: state.username,
           category: state.category,
           difficulty: state.difficulty
       }
```
La clase importara tendrá un dispatch del action LOGIN. Que enviará los datos guardados en el state del componente introducidos mediante el formulario con el método "handleChange"

El método sendData se encargará de llamar el método en props que activará el dispatch "LOGIN" y hacer un redirect al componente board.

#### board.js

Este componente contiene los dos componentes que explicaré más adelante QuizHeader y Quiz Question o QuizResult en caso de que el juego haya terminado.

La función de este componente es proveer a los componentes hijos de la información del store.

#### header.js

Este componente es una función que para abastecerse de un state utilizara "hooks". Estos hooks comprueban la puntuación con old y la gestión del modal con open que nos avisara si la respuesta ha sido correcta mostrando un mensaje flotante en pantalla

Además el componente nos mostrará el nombre de usuario en juego y nos mantendrá informados de la ronda y las respuestas acertadas

Este componente accede ni recoge información del store

#### question.js

La clase QuizQuestion, es un componente hijo del componente Board. Este tiene la función de mostrar los detalles de la pregunta.

La información de cada pregunta la obtiene del store, concretamente de currentQuestion y las respuestas posibles junto a la correcta mezcladas en un array llamada answers.
También tiene la función de lanzar la siguiente pregunta con un action llamado "NEXT".

El componente se encarga de comprobar si ha acertado la pregunta o no, y enviar ese resultado mediante la acción. Para que el middleware actúe en consecuencia.

Mediante el state controla que pregunta ha sido seleccionada y además de servir para la verificación modifica la "class" para resaltar la pregunta al usuario mediante CSS.

#### result.js

Este componente te muestra la puntuación una vez es llamado. Es función la tiene el componente board, por lo tanto solo
aparecerá cuando se acaben las rondas y mostrará el total de puntos/respuestas acertadas acumulado.

#### resultDiagog.js

Componente encargado de mostrar en un mensaje flotante. En esta app es utilizado por el header y solo es llamado cuando la respuestas ha sido acertada.


#### store/store.js

Store de toda la aplicación, único encargado de gestionar la información que va a ser enviada al resto de los componentes segun peticion. Se encargue que avisar a cada componente de que la información ha cambiado y de indicarle cual es.

Contiene los siguiente parámetros, que se irán modificando a lo largo del juego (no todos).

```javascript
let initialState = {
   username: '',
   category: 0,
   difficulty: 'easy',
   url: 'http://',
   questions: [],
   currentQuestion: {},
   answers: [],
   points: 0,
   gameRound: 0,
   ended: false
}
```

Maneja los siguientes casos:

   'LOGIN' -> Guarda los datos del login en el store
   'SET_QUESTIONS' -> Guarda todas las preguntas, respuesta de la petición HTTP al Api
   'CURRENT_QUESTION' -> Actualiza en el store la pregunta a mostrar, genera con dos métodos personalizados
       (@/utils/shuffle.js) las respuestas y añade en uno la ronda.
    'CORRECT_ANSWER' -> Añade un punto al total de respuestas acertadas
    'GAME_ENDED' -> Indica que el juego está finalizado y devuelve el contador de rondas a 0.
  
#### store/middleware.js

La cabeza de la aplicación. Que se encargará de modificar el store según las acciones que le llegue o el payload que contengan

El primer middleware se encarga de realizar la petición HTTP si el action es LOGIN. Y entonces lanzar dos actions mas con los datos de la respuesta.

El segundo middleware comprobará si con el action NEXT el juego ha terminado. Previamente se habrá modificado el marcador en función de si la respuesta del usuario es TRUE o FALSE

### Conclusiones
  
Una programación muy sencilla e intuitiva. La utilización de Redux ha simplificado mucho el código y se ha podido reutilizar métodos con mucha facilidad.
He aprendido a utilizar el middleware gracias a acabar de entender cuál era su función.

La aplicación ha cubierto mis expectativas y he disfrutado programando.
  
### Posibles mejoras

· Mostrar al final al usuario las respuestas verdaderas de todas las preguntas.
· Añadir un marcador de mejores puntuaciones entro todos los posibles jugadores.


