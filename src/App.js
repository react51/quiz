import React from 'react';
import './App.css';
import {Route, Switch} from 'react-router-dom';
import QuizBoard from "./components/board";
import QuizLogin from "./components/login";

function App() {

    return (
        <div className="App">
            <Switch>
                <Route exact path={"/"} component={QuizLogin}/>
                <Route exact path={"/board"} component={QuizBoard}/>
            </Switch>
        </div>
    );
}

export default App;
