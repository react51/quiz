import React from "react";
import {QuizHeader} from "./board/header";
import {connect} from "react-redux";
import QuizQuestion from "./board/question";
import QuizResult from "./board/result";


function mapStateToProps(state) {
    return {
        state: state
    }
}

class QuizBoard extends React.Component {

    render() {
        return (
            <div>
                <QuizHeader
                    username={this.props.state.username}
                    points={this.props.state.points}
                    round={this.props.state.gameRound }
                />
                {
                    /*muestra un componente u otro según si el juego esta finalizado o no*/
                    !this.props.state.ended ? (
                        <QuizQuestion/>
                    ) : (
                        <QuizResult result={this.props.state.points}/>
                    )
                }

            </div>
        );
    }
}

export default connect(mapStateToProps, null)(QuizBoard)
