import React, {useEffect, useState} from "react";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import SimpleDialog from "./resultDialog";


export function QuizHeader(props) {
    /*Hooks usados para poder setear constantes igual que un state en un controlador*/
    const [old, setOld] = useState(0)
    const [open, setOpen] = useState(false);

    /*abre el modal "Respuesta correcta!*/
    const handleClickOpen = () => {
        setOpen(true);
    };

    /*cierra el modal "Respuesta correcta!*/
    const handleClose = value => {
        setOpen(false);
    };

    /*funcion que se ejecuta a la carga del componente y al modificar el state o los props (ciclos de vida del componente)*/
    useEffect(() => {
        console.log('hola', props, old)
        if (props.points > old) {
            setOld(props.points)
            handleClickOpen()
        }

    })

    return (
        <div>
            <AppBar position={'static'} className={'headerContainer'}>
                <Grid
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="center"
                    >
                    <Typography>{props.username}</Typography>

                    <Typography>POINTS: {props.points || 0}</Typography>
                </Grid>
                <Typography className={'headerCenterChild'}>ROUND {props.round || 0} / {props.maxRound || 10}</Typography>
                <SimpleDialog result={'Correcto!!'} open={open} onClose={handleClose} />
            </AppBar>
        </div>
    )
}
