import React from "react";
import {connect} from "react-redux";

import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";

function mapStateToProps(state) {
    return {
        currentQuestion: state.currentQuestion,
        answers: state.answers
    }
}

function mapDispatchToProps(dispatch) {
    return {
        correct: (result) => dispatch({type: 'NEXT', payload: result})
    }
}


class QuizQuestion extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            answerSelected: ''
        }

    }

    useStyles = () => {
        return makeStyles({
            root: {
                minWidth: 600,
            },

            title: {
                fontSize: 14,
            },
            pos: {
                marginBottom: 12,
            }
        })
    }

    /*metodo generico para modificar cualquier elemento del state*/
    selected = (e) => {
        this.setState({
            answerSelected: this.props.answers[e.currentTarget.id]
        })
    }

    /*metodo que lanza el action NEXT*/
    checkAndSend = () => {
        this.props.correct(this.state.answerSelected === this.props.currentQuestion.correct_answer)
    }

    render() {
        let classes = this.useStyles()
        return (
            <div>
                <Card className={classes.root}>
                    <CardContent>
                        <Typography className={classes.title} variant={'h4'} gutterBottom>
                            {this.props.currentQuestion.category}
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            {this.props.currentQuestion.difficulty}
                        </Typography>
                        <Typography variant="h3" component="h2">
                            {this.props.currentQuestion.question}
                        </Typography>
                        {
                            this.props.answers.map((ans, index) =>
                                <div className={(ans === this.state.answerSelected) ? 'answers_checked' : 'answers'}
                                     onClick={this.selected}
                                     id={index}
                                     key={index}
                                ><Typography variant={'h5'}>{ans}</Typography></div>)
                        }

                    </CardContent>
                    {
                        this.state.answerSelected ? (<CardActions>
                                <Button size="large" color={'primary'} onClick={this.checkAndSend}>SEND</Button>
                            </CardActions>) :
                            null
                    }
                </Card>
            </div>
        )
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(QuizQuestion)
