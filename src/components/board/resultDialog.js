import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import React from "react";
import PropTypes from 'prop-types';


function SimpleDialog(props) {
    /*otra forma de manejar la informacion de los props y tener un codigo mas limpio*/
    const { onClose, result, open } = props;

    const handleClose = () => {
        /*metodo heredado*/
        onClose(result);
    };

    return (
        <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
            <DialogTitle id="simple-dialog-title">{result}</DialogTitle>
        </Dialog>
    );
}

SimpleDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    result: PropTypes.string.isRequired,
};


export default SimpleDialog
