import React from "react";
import {connect} from "react-redux";


import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import Select from '@material-ui/core/Select';
import MenuItem from "@material-ui/core/MenuItem";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Grid from "@material-ui/core/Grid";
import Button from '@material-ui/core/Button';



function mapStateToProps(state) {
    return {
        login: {
            username: state.username,
            category: state.category,
            difficulty: state.difficulty
        }
    }
}

function mapDispatchToProps(dispatch) {
    return {
        loginUserAndParams: (login) => dispatch({type: 'LOGIN', payload:login})
    }
}

class QuizLogin extends React.Component {

    constructor(state) {
        super(state);
        this.state = {
            username: '',
            category: '',
            difficulty: '',
        }
    }

    handleChange = (e, params = null) => {
        this.setState({
            [params]: e.target.value || e.target.name

        })
        console.log(this.state);
    }

    sendData = () => {
        if (this.state.username !== ''){
            this.props.loginUserAndParams(this.state)
            this.props.history.push('/board')
        }
    }


    useStyle() {
        return makeStyles(
            theme => ({
                formControl: {
                    margin: theme.spacing(1),
                    minWidth: 120,
                },
                selectEmpty: {
                    marginTop: theme.spacing(2),
                }
            })
        )
    }

    render() {
        const classes = this.useStyle()
        return (
            <div>
                <h1>Quiz</h1>
                <Grid
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                    spacing={3}
                >
                    <Grid item>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="name">Player Name</InputLabel>
                            <Input
                                id="username"
                                aria-describedby="my-helper-text"
                                value={this.state.name}
                                onChange={
                                    (event) => {
                                        this.handleChange(event, 'username')
                                    }
                                }
                            />
                        </FormControl>
                    </Grid>
                    <Grid item>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="category">Category</InputLabel>
                            <Select
                                style={{width: '200px'}}
                                labelId="category"
                                id="category"
                                value={this.state.category}
                                onChange={
                                    (event) => {
                                        this.handleChange(event, 'category')
                                    }
                                }
                            >
                                {/*las Values indican en la URL la categoria al API*/}
                                <MenuItem value={10}>Books</MenuItem>
                                <MenuItem value={11}>Films</MenuItem>
                                <MenuItem value={12}>Music</MenuItem>
                                <MenuItem value={15}>Video Games</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="difficulty">Difficulty</InputLabel>
                            <Select
                                style={{width: '200px'}}
                                labelId="category"
                                id="category"
                                value={this.state.difficulty}
                                onChange={
                                    (event) => {
                                        this.handleChange(event, 'difficulty')
                                    }
                                }
                            >
                                <MenuItem value={'easy'}>Easy</MenuItem>
                                <MenuItem value={'medium'}>Medium</MenuItem>
                                <MenuItem value={'hard'}>Hard</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item>
                        <Button
                            style={{width: '200px'}}
                            onClick={this.sendData}
                        >Start</Button>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(QuizLogin)
