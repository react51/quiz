import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
/*tipografia*/
import 'typeface-roboto';
/*libreria para el router*/
import {BrowserRouter, Route, Switch} from 'react-router-dom';
/*configuracion del store*/
import store from './store/store'
import {Provider} from "react-redux";

ReactDOM.render(
    (
        <Provider store={store}>
            <BrowserRouter>
                <Switch>
                    <Route path="/" component={App}/>
                </Switch>
            </BrowserRouter>
        </Provider>
    )
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
