import axios from 'axios'


const actionsMiddleware = store => next => action => {
    if (action.type === 'LOGIN') {
        axios.get(`https://opentdb.com/api.php?amount=10&category=${action.payload.category}&difficulty=${action.payload.difficulty}`).then(res => {
            store.dispatch({type: 'SET_QUESTIONS', payload: res.data.results})
            store.dispatch({type: 'CURRENT_QUESTION'})
        })
    }

    return next(action)
}

const checkCorrectAnswer = store => next => action => {

    if (action.type === 'NEXT') {
        if (action.payload) {
            store.dispatch({type: 'CORRECT_ANSWER'})
        }
        if (store.getState().gameRound === 10) {
            store.dispatch({type:'GAME_ENDED'})
            return next(action)
        }
        store.dispatch({type: 'CURRENT_QUESTION'})
    }
    return next(action)
}


export { actionsMiddleware, checkCorrectAnswer }
