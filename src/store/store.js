import {applyMiddleware, createStore, compose} from "redux";
import {actionsMiddleware, checkCorrectAnswer} from "./middleware";
import {shuffle, mergeAnswers} from "../utils/shuffle";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

let initialState = {
    username: '',
    category: 0,
    difficulty: 'easy',
    url: 'http://',
    questions: [],
    currentQuestion: {},
    answers: [],
    points: 0,
    gameRound: 0,
    ended: false
}

function QuizReducers(state = initialState, action) {
    switch (action.type) {
        case 'LOGIN':
            console.log(action.payload)
            return {
                ...state,
                username: action.payload.username,
                category: action.payload.category,
                difficulty: action.payload.difficulty
            }
        case 'SET_QUESTIONS':
            return {
                ...state,
                questions: action.payload
            }
        case 'CURRENT_QUESTION':
            return {
                ...state,
                currentQuestion: state.questions[state.gameRound],
                answers: shuffle(mergeAnswers(state.questions[state.gameRound].correct_answer,state.questions[state.gameRound].incorrect_answers)),
                gameRound: ++state.gameRound
            }
        case 'CORRECT_ANSWER':
            return {
                ...state,
                points: ++state.points,
            }
        case 'GAME_ENDED':
            return {
                ...state,
                ended: true,
                gameRound: 0
            }
        default:
            return state

    }
}

export default createStore(
    QuizReducers,
    composeEnhancers(applyMiddleware(actionsMiddleware, checkCorrectAnswer))
)
