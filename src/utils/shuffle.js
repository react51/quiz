export function mergeAnswers(correct, incorrect) {
    let array = incorrect
    array.push(correct)

    console.log(array);

    return array.filter((a, b) => array.indexOf(a) === b)
}

export function shuffle(array) {
    console.log(array);
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}
